class AddSpreeLevelToSpreeUser < ActiveRecord::Migration[5.1]
  def change
    add_column :spree_users, :spree_level_id,:integer
    add_index :spree_users, :spree_level_id
  end
end