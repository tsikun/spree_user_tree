class AddAncestryToSpreeUser < ActiveRecord::Migration[5.1]
  def change
    add_column :spree_users, :ancestry, :string
    add_index :spree_users, :ancestry
  end
end
