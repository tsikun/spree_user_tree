class AddUplevelDownLevelToSpreeLevel < ActiveRecord::Migration[5.1]
  def change
    add_column :spree_levels,:upper_level_id,:integer
    add_index :spree_levels,:upper_level_id
    add_column :spree_levels,:lower_level_id,:integer
    add_index :spree_levels,:lower_level_id
  end
end
