Deface::Override.new(
  virtual_path: 'spree/layouts/admin',
  name: 'levels_admin_sidebar_menu',
  insert_bottom: '#main-sidebar',
  partial: 'spree/admin/shared/levels_sidebar_menu'
)
