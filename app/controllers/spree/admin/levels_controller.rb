module Spree
  module Admin
class LevelsController < ResourceController
  before_action :set_spree_level, only: [:show, :edit, :update, :destroy]

  # GET /spree/levels
  def index
    @spree_levels = Spree::Level.all
  end

  # GET /spree/levels/1
  def show
  end

  # GET /spree/levels/new
  def new
    @spree_level = Spree::Level.new
  end

  # GET /spree/levels/1/edit
  def edit
  end

  # POST /spree/levels
  def create
    @spree_level = Spree::Level.new(spree_level_params)

    if @spree_level.save
      redirect_to @spree_level, notice: 'Level was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /spree/levels/1
  def update
    if @spree_level.update(spree_level_params)
      redirect_to @spree_level, notice: 'Level was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /spree/levels/1
  def destroy
    @spree_level.destroy
    redirect_to spree_levels_url, notice: 'Level was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_spree_level
      @spree_level = Spree::Level.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def spree_level_params
      params.require(:spree_level).permit(:name)
    end
end
  end
end
