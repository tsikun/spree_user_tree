class Spree::Level < ApplicationRecord
  has_many :user,class_name: "Spree::User"
  has_one :upper_level,class_name: "Spree::Level", foreign_key: "upper_level_id"
  has_one :lower_level,class_name: "Spree::Level", foreign_key: "lower_level_id"
end