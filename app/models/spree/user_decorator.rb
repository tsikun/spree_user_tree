require 'ancestry'
Spree::User.class_eval do
  belongs_to :level,class_name: "Spree::Level", foreign_key: :spree_level_id
  has_ancestry
end