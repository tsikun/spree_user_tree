FactoryBot.define do
  # Define your Spree extensions Factories within this file to enable applications, and other extensions to use and override them.
  #
  # Example adding this to your spec_helper will load these Factories for use:
  # require 'spree_user_tree/factories'

    factory :level ,class: Spree::Level do
        name "level1"
    end  
end
