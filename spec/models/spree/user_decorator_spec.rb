require 'spec_helper'

describe Spree::User do
    before(:all) { Spree::Role.create name: 'admin' }
    
    it "is testing factory_bot" do
        user = build(:user)
        expect(user.save).to be true
    end  
    it "can create children" do
        user=create(:user)
        child=create(:user)
        child.parent=user
        child.save
        user.save
        expect(user.children.count).to eq(1)
    end
    it "can be assigned level" do
        user=build(:user)
        level=build(:level,name: 'custom_level')
        user.level=level
        level.save
        user.save
        expect(user.level.name).to eq("custom_level")
    end
    it "get children by level" do
        user=create(:user)
        level2=create(:level,name: 'leve2')
        level3=create(:level,name: 'leve3')
        2.times do
            child=build(:user)
            child.level=level2
            child.parent=user
            child.save            
        end
        3.times do
            child=build(:user)
            child.level=level3
            child.parent=user
            child.save            
        end
        user.save
        expect(user.descendants.count).to eq(5)
        expect(user.descendants.where(:level=> level2).count).to eq(2)
    end 
end