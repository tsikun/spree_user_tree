require "spec_helper"

# RSpec.describe Spree::LevelsController, type: :routing do
#   describe "routing" do

#     it "routes to #index" do
#       expect(:get => "/spree/levels").to route_to("spree/levels#index")
#     end

#     it "routes to #new" do
#       expect(:get => "/spree/levels/new").to route_to("spree/levels#new")
#     end

#     it "routes to #show" do
#       expect(:get => "/spree/levels/1").to route_to("spree/levels#show", :id => "1")
#     end

#     it "routes to #edit" do
#       expect(:get => "/spree/levels/1/edit").to route_to("spree/levels#edit", :id => "1")
#     end

#     it "routes to #create" do
#       expect(:post => "/spree/levels").to route_to("spree/levels#create")
#     end

#     it "routes to #update via PUT" do
#       expect(:put => "/spree/levels/1").to route_to("spree/levels#update", :id => "1")
#     end

#     it "routes to #update via PATCH" do
#       expect(:patch => "/spree/levels/1").to route_to("spree/levels#update", :id => "1")
#     end

#     it "routes to #destroy" do
#       expect(:delete => "/spree/levels/1").to route_to("spree/levels#destroy", :id => "1")
#     end

#   end
# end
